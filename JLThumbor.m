//
//  JLThumbor.m
//  JLThumbor
//
//  Created by Jason Lagaac on 7/04/2014.
//  Copyright (c) 2014 Jason Lagaac. All rights reserved.
//

#import "JLThumbor.h"
#import "MF_Base64Additions.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>

// Encode a string to embed in an URL.
NSString* encodeToPercentEscapeString(NSString *string) {
  return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                          (CFStringRef) string,
                                          NULL,
                                          (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                          kCFStringEncodingUTF8));
}



@interface JLThumbor ()

/**
 The Thumbor server URL
 */
@property (nonatomic, strong) NSString *serverUrl;

/**
 The Thumbor server secret key
 */
@property (nonatomic, strong) NSString *key;

@end

@implementation JLThumbor


- (id)initWithURL:(NSString *)thumborServerURL
{
    if (self = [super init]) {
        self.serverUrl = thumborServerURL;
    }

    return self;
}

- (id)initWithURL:(NSString *)thumborServerURL
              key:(NSString *)thumborKey
{
    if (self = [super init]) {
        self.serverUrl = thumborServerURL;
        self.key = thumborKey;
    }

    return self;
}

- (NSString *)buildImageWithWidth:(NSInteger)width
                       height:(NSInteger)height
                     imageURL:(NSString *)imageURL
{
    return [self buildImageWithWidth:width
                              height:height
                             filters:nil
                            imageURL:imageURL];
}

- (NSString *)buildImageWithWidth:(NSInteger)width
                       height:(NSInteger)height
                      filters:(NSString *)filters
                     imageURL:(NSString *)imageURL
{
    return [self buildImageWithWidth:width
                              height:height
                                crop:nil
                       verticalAlign:kVerticalNone
                     horizontalAlign:kHorizontalNone
                             filters:filters
                                trim:YES
                               smart:NO
                               fitIn:YES
                            imageURL:imageURL];
}

- (NSString *)buildImageWithWidth:(NSInteger)width
                       height:(NSInteger)height
                         crop:(NSString *)crop
                verticalAlign:(VerticalAlign)vAlign
              horizontalAlign:(HorizontalAlign)hAlign
                      filters:(NSString *)filters
                         trim:(BOOL)isTrim
                        smart:(BOOL)isSmart
                        fitIn:(BOOL)isFitIn
                     imageURL:(NSString *)imageURL
{
    NSMutableString *resultString;
    NSMutableString *operationString = [NSMutableString new];


    // Determine if trim option is enabled
    if (isTrim) {
        [operationString appendString:@"trim/"];
    }

    if (crop) {
        [operationString appendString:[NSString stringWithFormat:@"%@/", crop]];
    }

    // Determine if the fit-in opertion is enabled
    if (isFitIn) {
        [operationString appendString:@"fit-in/"];
    }

    // Determine if the image is to be resized with a width and height
    if (width && height) {
        [operationString appendString:[NSString stringWithFormat:@"%dx%d/", width, height]];
    }


    // Determine the horizontal alignment
    if (hAlign) {
        switch (hAlign) {
            case kHorizontalLeft:
                [operationString appendString:@"left/"];
                break;
            case kHorizontalCenter:
                [operationString appendString:@"center/"];
                break;
            case kHorizontalRight:
                [operationString appendString:@"right/"];
                break;
            default:
                break;
        }
    }

    // Determine the vertical alignment
    if (vAlign) {
        switch (vAlign) {
            case kVerticalTop:
                [operationString appendString:@"top/"];
                break;
            case kVerticalMiddle:
                [operationString appendString:@"middle/"];
                break;
            case kVerticalBottom:
                [operationString appendString:@"bottom/"];
                break;
            default:
                break;
        }
    }

    if (filters) {
        [operationString appendString:[NSString stringWithFormat:@"filters:%@/", filters]];
    }

    // Encode the image URL
    NSString *encodedURL = encodeToPercentEscapeString(imageURL);
    [operationString appendString:encodedURL];

    if (self.key) {
        // Generate secure thumbor URL if key is provided
        const char *cKey = [self.key cStringUsingEncoding:NSASCIIStringEncoding];
        const char *cData = [operationString cStringUsingEncoding:NSASCIIStringEncoding];
        unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];

        CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);

        NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC
                                              length:sizeof(cHMAC)];

        NSString *escapedHash = [[HMAC base64String] stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
        escapedHash = [escapedHash stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
        resultString = [NSMutableString stringWithFormat:@"%@/%@/%@", self.serverUrl, escapedHash, operationString];
    } else {
        // Generate unsecure thumbor URL
        resultString = [NSMutableString stringWithFormat:@"%@/unsafe/%@", self.serverUrl, operationString];
    }

    return resultString;
}


@end

JLThumbor - Objective-C Thumbor Client
======================================

Objective-C client of the [Thumbor Image Service](https://github.com/globocom/thumbor)

This implementation is compatible with iOS and Mac OSX

Examples
=========

Generating an instance
--------------

```objective-c
// Without Encryption
JLThumbor *thumbor = [[JLThumbor alloc] initWithURL:@"http://example-server.com"];

// With Encryption
JLThumbor *thumbor = [[JLThumbor alloc] initWithURL:@"http://example-server.com"
                                                key:@"deadbeef"];
```

Simple Resize
-------------
```objective-c
[thumbor buildImageWithWidth:300
                      height:300
                    imageURL:@"http://example.com/image.jpg"];
```

Resize and Smart Crop
---------------------
```objective-c
[tumbor buildImageWithWidth:300
                     height:300
                       crop:@"5x5:195x195"
              verticalAlign:kVerticalNone
            horizontalAlign:kHorizontalNone
                    filters:nil
                       trim:NO
                      smart:YES
                      fitIn:YES
                   imageURL:@"http://example.com/image.jpg"];
```


Resize and Crop with Gravity
---------------------
```objective-c
[tumbor buildImageWithWidth:300
                     height:300
                       crop:@"5x5:195x195"
              verticalAlign:kVerticalBottom
            horizontalAlign:kHorizontalRight
                    filters:nil
                       trim:NO
                      smart:YES
                      fitIn:YES
                   imageURL:@"http://example.com/image.jpg"];

```

Resize with filters
---------------------
```objective-c
[tumbor buildImageWithWidth:300
                     height:300
                       crop:nil
              verticalAlign:kVerticalNone
            horizontalAlign:kHorizontalNone
                    filters:@"quality(95):format(webp)"
                       trim:YES
                      smart:NO
                      fitIn:NO
                   imageURL:@"http://example.com/image.jpg"];

```

//
//  JLThumbor.h
//  JLThumbor
//
//  Created by Jason Lagaac on 7/04/2014.
//  Copyright (c) 2014 Jason Lagaac. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Vertial aligment options
 */

typedef enum {
    kVerticalNone,
    kVerticalTop,
    kVerticalMiddle,
    kVerticalBottom
} VerticalAlign;


/**
 Horizontal aligment options
 */

typedef enum {
    kHorizontalNone,
    kHorizontalLeft,
    kHorizontalCenter,
    kHorizontalRight
} HorizontalAlign;


@interface JLThumbor : NSObject

/** 
 Initialise unsafe URL generation
 @param thumborServerURL url of the main server
 @returns new instance of JLThumbor
 */
- (id)initWithURL:(NSString *)thumborServerURL;


/**
 Initialise unsafe URL generation
 @param thumborServerURL url of the main server
 @param thumborKey secret key to sign requests
 @returns new instance of JLThumbor
 */
- (id)initWithURL:(NSString *)thumborServerURL
              key:(NSString *)thumborKey;


/**
 Initialise unsafe URL generation
 @param width horizontal size of the new image
 @param height vertical size of the new image
 @param imageURL URL of the image to be built
 @returns image built with paramters
 */

- (NSString *)buildImageWithWidth:(NSInteger)width
                           height:(NSInteger)height
                         imageURL:(NSString *)imageURL;

/**
 Initialise unsafe URL generation
 @param width horizontal size of the new image
 @param height vertical size of the new image
 @param filters thumbor processing filters
 @param imageURL URL of the image to be built
 */


- (NSString *)buildImageWithWidth:(NSInteger)width
                           height:(NSInteger)height
                          filters:(NSString *)filters
                         imageURL:(NSString *)imageURL;

/**
 Initialise unsafe URL generation
 @param width horizontal size of the new image
 @param height vertical size of the new image
 @param crop thumbor cropping dimensions
 @param vAlign vertial alignment
 @param hAlign horizontal alignment
 @param isTrim trim action enabled
 @param isSmart smart action enabled
 @param fitIn fit the image into container
 @param imageURL URL of the image to be built
 */


- (NSString *)buildImageWithWidth:(NSInteger)width
                           height:(NSInteger)height
                             crop:(NSString *)crop
                    verticalAlign:(VerticalAlign)vAlign
                  horizontalAlign:(HorizontalAlign)hAlign
                          filters:(NSString *)filters
                             trim:(BOOL)isTrim
                            smart:(BOOL)isSmart
                            fitIn:(BOOL)isFitIn
                         imageURL:(NSString *)imageURL;


@end
